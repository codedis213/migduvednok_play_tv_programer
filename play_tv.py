from req_module import *
from req_module import *
from bs4 import BeautifulSoup
from thread_queue_module import *
import requests

class PlayTvProgrammer(RequestSet, MyThread):

    def __init__(self, domain, homelink, pre_dir, proxy_file, thread_quantity):
        self.domain = domain
        self.homelink = homelink
        RequestSet. __init__(self,pre_dir, proxy_file)
        MyThread.__init__(self, thread_quantity)

    def req_module_json(self, url):
        for idx in xrange(3):
            host = self.proxy_spilt()

            proxies = {"http": "http://%s/" % host}

            if url.startswith("https:"):
                url = "http://" + url[8:]
                self.headers["x-crawlera-use-https"] = "1"

            try:
                logging.debug(host)
                res = requests.get(url, headers=self.headers, proxies=proxies, timeout=15,
                                     auth=self.auth, allow_redirects=False)
                json_dict = res.json()
                res.close()
                return json_dict

            except:
                pass

        return None

    def programme_series(self):
        link = "http://r7.tv2.dk/api/3/series/order-popularity/category-2/ApplicationCode-Play_Web/" \
               "display_context-midres/limit-20/offset-0.json"
        json_dict = self.req_module_json(link)
        total = json_dict.get("total")
        print total

        link = "http://r7.tv2.dk/api/3/series/order-popularity/category-2/ApplicationCode-Play_Web/" \
               "display_context-midres/limit-%s/offset-0.json"%(total)
        json_dict = self.req_module_json(link)

        print json_dict.get("series")
        

def start_main():
    pre_dir = "migduvednok_play_tv_programer"
    proxy_file = "proxies.txt"
    domain = "http://play.tv2.dk/"
    homelink = "http://play.tv2.dk/programmer/"
    thread_quantity = 5

    obj = PlayTvProgrammer(domain, homelink, pre_dir, proxy_file, thread_quantity)
    obj.proxy_to_list()

    obj.programme_series()




if __name__=="__main__":
    start_main()
