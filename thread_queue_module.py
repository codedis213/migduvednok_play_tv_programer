from threading import Thread
from Queue import Queue
import time
import logging

logging.basicConfig(level=logging.DEBUG, format='[%(levelname)s] (%(threadName)-10s) %(message)s', )


class MyThread(object):
    """
    obj = MyThread(5)
    iter_seq = list(range(10))
    obj.start_thread(iter_seq, obj.work_on_elem)
    """

    thread_quantity = 5
    enclosure_queue2 = Queue()

    def __init__(self, thread_quantity=5):
        MyThread.thread_quantity = thread_quantity
        self.t_list = []

    def start_thread(self, iter_seq, fun_name):
        thread_quantity = MyThread.thread_quantity
        enclosure_queue2 = MyThread.enclosure_queue2
        t_list = self.t_list

        for i in range(thread_quantity):
            t_list.append(Thread(target=self.target_thread, args=(i, enclosure_queue2)))
            t_list[-1].start()

        for elem in iter_seq:
            enclosure_queue2.put((elem, fun_name))

        enclosure_queue2.join()

        for p in t_list:
            enclosure_queue2.put(None)

        enclosure_queue2.join()

        for p in t_list:
            p.join(180)


    def target_thread(self, i, q):
        for elem, fun_name in iter(q.get, None):
            try:
                fun_name(elem)

            except:
                pass

            time.sleep(2)
            q.task_done()

        q.task_done()


    def work_on_elem(self, elem):
        logging.debug(elem)


def startmain():
    obj = MyThread(5)
    iter_seq = list(range(20))
    obj.start_thread(iter_seq, obj.work_on_elem)


if __name__=="__main__":
    startmain()